--TRIGGER-------------------------------------------------------------
--un trigger si attiva a fronte di un dato evento e, se sussiste una data condizione, allora esegue una data azione

CREATE TRIGGER EmpSalaryTooHigh		--trigger che controlla se il salario aggiornato all'impiegato è troppo elevato, allora rimette quello vecchio
AFTER UPDATE OF Salary ON Employee    -- evento
REFERENCING NEW AS N OLD AS O
FOR EACH ROW
WHEN (N.Salary > (SELECT Salary FROM Employee WHERE EmpCode = N.EmpManager))   -- condizione
UPDATE Employee      -- azione
SET Salary = O.Salary
WHERE EmpCode = N.EmpCode

--un trigger fa sempre riferimento a una singola tabella (target); nell’esempio Employee
--un evento è un istruzione SQL di manipolazione dei dati (INSERT, DELETE, UPDATE); nell’esempio è UPDATE
--una condizione è un predicato booleano; nell’esempio N.Saraly > (SELECT …)
--un’azione è una sequenza di una o più istruzioni SQL; nell’esempio le ultime tre righe
--se sono presenti più azioni vengono eseguite in maniera atomica con:
BEGIN ATOMIC <istr1>; <istr2>; END.

/*ROW*/
--nel caso di eventi che coinvolgano più tuple della tabella target, un trigger può essere attivato come:

FOR EACH ROW --row trigger: "per ognuna di queste tuple" ad es. per controllare che tutti gli update siano legali
FOR EACH STATEMENT --statement trigger "solo una volta per data istruzione ()" di fatto lavorando in modo aggregato, ad es. per contare quanti inserimenti sono stati eseguiti

/*VARIABILI*/
--un trigger ha spesso necessità di fare riferimento allo stato del DB prima e/o dopo l’evento

OLD --valore della tupla prima della modifica (ha senso in caso di INSERT)
NEW --valore della tupla dopo la modifica (non ha senso in caso di DELETE)
OLD_TABLE --una ipotetica table che contiene tutte le tuple modificate, con i valori prima della modifica (ha senso in caso di INSERT)
NEW_TABLE --idem, ma dopo la modifica (non ha senso in caso di DELETE)

/*BEFORE TRIGGER*/
--usati per far rispettare i vincoli di integrità prima che l'evento accada. L’istruzione SIGNAL annulla gli effetti dell’evento che ha attivato il trigger

CREATE TRIGGER CheckEmpSalary	--trigger che controlla se il salario aggiornato all'impiegato è troppo elevato, allora non lo cambia e manda un messaggio di sistema
NO CASCADE BEFORE INSERT ON Employee
REFERENCING NEW AS NewEmp
FOR EACH ROW
WHEN (NewEmp.Salary > (SELECT Salary FROM Employee WHERE EmpCode = NewEmp.EmpManager))
SIGNAL SQLSTATE '70000' ('Stipendio troppo elevato!')
--before trigger è necessariamente un row tigger, e può usare solo le variabili NEW e OLD. Inoltre non può includere azioni che modifichino il DB
--le azioni di un before trigger possono essere: SELECT, SIGNAL, SET

--i before trigger possono essere efficacemente usati per far rispettare vincoli persi nella traduzione da schema concettuale a schema relazionale. Ad esempio per i vincoli di mutua esclusione:
CREATE TRIGGER DisjointFromErasmus --le matricole degli studenti iscritti devono essere distinte da quelle degli studenti Erasmus.
NO CASCADE BEFORE INSERT ON StudIscritti
REFERENCING NEW AS NewStud
FOR EACH ROW
WHEN(EXISTS(SELECT * FROM StudErasmus E
WHERE E.Matr = NewStud.Matr))
SIGNAL SQLSTATE '70000' (‘Matricola già presente!’)

/*AFTER TRIGGER*/
--usati per far rispettare i vincoli di integrità dopo che l'evento accade reagendo alla modifica del DB
--un after trigger può includere le seguenti azioni: SELECT, INSERT, DELETE, UPDATE, SIGNAL.

CREATE TRIGGER UpdateQtaResidua --aggiorna la quantità residua di tutti i prodotti venduti inseriti in una nuova fattura
AFTER INSERT ON Vendite
REFERENCING NEW AS NuovaVendita
FOR EACH ROW
UPDATE Giacenze
SET QtaResidua = Qtaresidua – NuovaVendita.Quantita
WHERE CodProd = NuovaVendita.CodProd

/*CASCATA DI TRIGGER*/

CREATE TRIGGER InsertQtaDaOrdinare --se la giacenza di un prodotto è minore del minimo provvede a inserire una tupla in CARENZE
AFTER UPDATE ON Giacenze
REFERENCING NEW AS NG
FOR EACH ROW
WHEN (NG.QtaResidua < NG.ScortaMinima)
INSERT INTO CARENZE
VALUES (NG.CodProd,NG.SCortaMinima-NG.QtaResidua)

/*CICLI DI TRIGGER*/

CREATE TRIGGER T1
AFTER INSERT INTO R 
REFERENCING NEW AS N 
FOR EACH ROW 
DELETE FROM R 
WHERE id=N.id 

CREATE TRIGGER T2 
AFTER DELETE FROM R 
REFERENCING OLD AS O 
FOR EACH ROW 
INSERT INTO R VALUES (O.id,…)

--i cicli si possono presentare anche con un solo trigger (ricorsivo, in quanti ri-attiva se stesso)
CREATE TRIGGER CheckSalariesWrong 
AFTER UPDATE OF Salary ON Employee 
FOR EACH ROW 
WHEN ((SELECT AVG(Salary) FROM Employee) > 1000) 
UPDATE Employee 
SET Salary = 1.1*Salary

/*COSTRUTTO IF...THEN..[ELSE]...END IF*/

CREATE TRIGGER DisjointFromErasmus 
NO CASCADE BEFORE INSERT ON StudIscritti 
REFERENCING NEW AS NewStud 
FOR EACH ROW 
IF (EXISTS(SELECT * FROM StudErasmus E WHERE E.Matr = NewStud.Matr)) THEN 
	SIGNAL SQLSTATE '70000' (‘Matricola già presente!’); 
END IF

/*AZIONI ALTERNATIVE*/
CREATE TRIGGER DUEAZIONI 
NO CASCADE BEFORE INSERT ON Employee 
REFERENCING NEW AS N 
FOR EACH ROW 
IF (N.CodImp > 100) THEN 
	SIGNAL SQLSTATE ‘80000’ (‘Codice invalido’); 
ELSE SET N.Salary = (SELECT MIN(Salary) FROM Employee WHERE Dept = N.Dept); 
END IF

--questo trigger o segnala errore oppure provvede a completare la tupla dell’impiegato inserito

/*COSTRUTTO IF THEN ELSE*/
CREATE TRIGGER upd_check BEFORE UPDATE ON account
FOR EACH ROW
BEGIN
        IF NEW.amount < 0 THEN
                SET NEW.amount = 0;
        ELSEIF NEW.amount > 100 THEN
                SET NEW.amount = 100;
        END IF;
END;