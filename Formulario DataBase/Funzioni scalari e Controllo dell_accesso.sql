--FUNZIONI SCALARI E CONTROLLO DELL'ACCESSO-------------------------------------------------------------

/*FUNZIONI SCALARI*/
--funzioni a livello di ennupla che restituiscono singoli valori

SELECT COALESCE (list)... --restituisce il primo valore non nullo nella lista
SELECT NULLIF (input,value)... --restituisce il valore nullo se l’espressione input ha come valore value, altrimenti restituisce input
CASE WHEN condizione THEN espr : .... ELSE espr END --restituisce l’espressione associata alla prima condizione soddisfatta

--Funzioni matematiche: abs(), degrees(), exp(), ln(), log(), radians(), round(), sqrt(), trunc(), float(), integer(), acos(), asin(), atan(), cos(), cot(), sin(), tan()
--Funzioni su stringhe: char_length(), lower(), upper(), substring(), trim()

/*PRIVILEGI*/
--in SQL ogni operazione deve essere autorizzata, è dunque possibile specificare chi (utente) e come (lettura, scrittura, …) può utilizzare la base di dati (o parte di essa)
--un utente predefinito _system (amministratore della base di dati) ha tutti i privilegi, mentre il creatore di una risorsa ha tutti i privilegi su di essa
--un principio fondamentale è che un utente che ha ricevuto un certo privilegio può a sua volta accordarlo ad altri utenti solo se è stato esplicitamente autorizzato a farlo

GRANT DBADM ON DATABASE TO Pippo WITH GRANT OPTION; --autorizza l’utente Pippo a passare l’autorità ad altri utenti
--SYSADM include quella DBADM, è dunque possibile passare quest'ultima ad altri utenti

--GRANT DATABASE
GRANT ALTERIN --concedi il priblegi di alterazione DB 
ON SCHEMA Magazzino --dal db Magazzino  
TO Magazziniere --al magazziniere, o PUBLIC
WITH GRANT OPTION --l'utente potrà inoltre concedere l'autorizzazione specificata ad altri utenti
--Possibili privilegi:
--CREATEIN: per creare oggetti (tables,views) nello schema
--ALTERIN: per modificare la struttura di tables dello schema
--DROPIN: per eliminare oggetti dallo schema

--GRANT TABLE
GRANT UPDATE --concedi il priblegi di aggiornamento DB 
ON TABLE Depositi --dal db Depositi  
TO Gestore --al gestore, o PUBLIC
--Possibili privilegi: CONTROL (privilegio master), ALTER, DELETE, INSERT, SELECT, INDEX, REFERENCES e UPDATE

--REVOKE
REVOKE { ALL | < lista di privilegi > } 
ON [ TABLE ] <table name> 
FROM { <lista di utenti e gruppi> | PUBLIC } 
[ restrict | cascade ]
--necesità dell'autorità SYSADM o DBADM, o CONTROL sulla relazione
